-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 19, 2019 at 07:13 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cvjecarna`
--
CREATE DATABASE cvjecarna;
USE cvjecarna;
-- --------------------------------------------------------

--
-- Table structure for table `cvjecarna`
--

CREATE TABLE `cvjecarna` (
  `ID` int(3) NOT NULL,
  `buket` varchar(50) COLLATE utf8_croatian_ci NOT NULL,
  `ime` varchar(30) COLLATE utf8_croatian_ci NOT NULL,
  `prezime` varchar(20) COLLATE utf8_croatian_ci NOT NULL,
  `adresa` varchar(40) COLLATE utf8_croatian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

--
-- Dumping data for table `cvjecarna`
--

INSERT INTO `cvjecarna` (`ID`, `buket`, `ime`, `prezime`, `adresa`) VALUES
(1,'1', 'Ana', 'Horvat', 'Zagrebacka 3'),
(2,'1', 'Iva', 'Irić', 'Istarska 6'),
(3,'3', 'Marija', 'Pop', 'Ulica grada Vukovara'),
(4,'1', 'Ines', 'Nenadić', 'Ulica  grada Mainza '),
(5,'2', 'Janja', 'Vrbič', 'Kneza Branimira 8');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cvjecarna`
--
ALTER TABLE `cvjecarna`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cvjecarna`
--
ALTER TABLE `cvjecarna`
  MODIFY `ID` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
