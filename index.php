<!DOCTYPE html>
<html>
<head>

<!-- <div class="header">

  <div class="info">
  	<br>
	<br>
    <h1 style="font-size: 70px;">Cvjecarna</h1>
    <br>
    <br>
    <h2>"Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit..."</h2>
    <div class="meta">
    </div>
  </div>
</div> -->
<!-- <section class="content">
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In nisl turpis, porttitor et finibus id, viverra a metus. Praesent non ante sed orci posuere varius quis sit amet dui. Cras molestie magna orci, id gravida dolor molestie in. Duis sollicitudin turpis quis tortor egestas, ut ultrices nisl elementum. Vestibulum sed ipsum eget nulla laoreet cursus in ac sem. Integer a suscipit justo, quis aliquam sapien. Maecenas et tellus nibh. Vivamus tincidunt eros id commodo pellentesque.</p>
</section> -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="style.css">
	<meta charset="utf-8">
	<base href="http://localhost/cvijece-forma/" target="_blank">
	<link rel="icon" type="image/png" href="/icon.png">
</head>
<body>

<header><center><h1>Forma</h1></center></header>
<main>

	<div class="container">
		<form id="form" method="post" action="obrada.php">
			<label>Odaberite buket: </label>
			<br>
			<label>Opcija 1 - ruzicasti buket</label>
			<input type="radio" name="buket" value="1" required="required" >
			<label>Opcija 2 - zuti buket</label>
			<input type="radio" name="buket" value="2" required="required" >
			<label>Opcija 3 - plavi buket</label>
			<input type="radio" name="buket" value="3" required="required" >
			<br>
			<label>Ime: </label>
				<input type="text" name="ime"><br>
				<label>Prezime: </label>
				<input type="text" name="prezime"><br>
				<label>Adresa: </label>
				<input type="text" name="adresa"><br>
				<br>
				<input type="submit" name="submit" value="submit" required="required">
		</form>
	<!-- <br>
	<div class="card-deck">
  <div class="card">
    <img class="card-img-top" src="roze.jpg" alt="Card image cap" style="background-color: #DEEAFE;">
    <div class="card-body">
      <h5 class="card-title">Ruzicasti buket</h5>


    </div>
  </div>
  <div class="card">
    <img class="card-img-top" src="zute.jpg" alt="Card image cap" style="background-color: #DEEAFE;">
    <div class="card-body">
      <h5 class="card-title">Zuti buket</h5>


    </div>
  </div>
  <div class="card">
    <img class="card-img-top" src="plave.jpg" alt="Card image cap" style="background-color: #DEEAFE;">
    <div class="card-body">
      <h5 class="card-title">Plavi buket</h5>

    </div>
  </div>
</div> -->
	<br>
	<hr>
	<a href="http://localhost/cvijece-forma">Početna</a>
	<p>Pretraži po imenu i prezimenu</p>
	<form method="get" action="">
		<input type="text" name="search" class="form-control">
		<input type="submit" class="btn btn-primary" value="Pretraži">
	</form>
	<table class="table">
		<tr>
			<th>ID</th>
			<th>Buket</th>
			<th>Ime</th>
			<th>Prezime</th>
			<th>Adresa</th>
			<th>Opcije</th>
		</tr>
		<?php
			include('db.php');

			$sql_full="SELECT
				cvjecarna.ID,
				cvjecarna.ime,
				cvjecarna.prezime,
				cvjecarna.adresa,
				buketi.naziv AS buket
				FROM cvjecarna
				LEFT JOIN buketi ON buketi.id=cvjecarna.buketi_id";
			$sql_single="SELECT
				cvjecarna.ID,
				cvjecarna.ime,
				cvjecarna.prezime,
				cvjecarna.adresa,
				buketi.naziv AS buket
				FROM cvjecarna
				LEFT JOIN buketi ON buketi.id=cvjecarna.buketi_id
				WHERE cvjecarna.ID=".$_GET["id"];
			$sql_search="SELECT
				cvjecarna.ID,
				cvjecarna.ime,
				cvjecarna.prezime,
				cvjecarna.adresa,
				buketi.naziv AS buket
				FROM cvjecarna
				LEFT JOIN buketi ON buketi.id=cvjecarna.buketi_id
				WHERE cvjecarna.ime LIKE '%".$_GET["search"]."%'
				OR cvjecarna.prezime LIKE '%".$_GET["search"]."%'";

			if ($_GET["id"] != '') {
				$sql = $sql_single;
			} else if ($_GET["search"] != '') {
				$sql = $sql_search;
			}	else {
				$sql = $sql_full;
			}

			$result = mysqli_query($link, $sql);

			if (mysqli_num_rows($result) > 0) {
				// output data of each row
				while($row = mysqli_fetch_assoc($result)) {
					echo '<tr>';
					echo "<td>".$row["ID"]. "</td>";
					if ($row["buket"] == null) {
						echo "<td>Buket nije odabran</td>";
					} else {
						echo "<td>".$row["buket"]. "</td>";
					}
					echo "<td>".$row["ime"]. "</td>";
					echo "<td>".$row["prezime"]. "</td>";
					if ($row["adresa"] == null) {
						echo "<td>/</td>";
					} else {
						echo "<td>".$row["adresa"]. "</td>";
					}
					echo "<td><a href='http://localhost/cvijece-forma/index.php?id=".$row["ID"]."'>Otvori</a></td>";
					echo '</tr>';
				}
			} else {
				echo "0 results";
			}
		?>
	</table>
	</div>
</main>

<footer>
	<div class="footer">
		<div id="button"></div>
		<div id="container">
			<div id="cont">
				<div class="footer_center">
					<h3>Made by Karla</h3>
				</div>
			</div>
		</div>
	</div>
</footer>
</body>
</html>
